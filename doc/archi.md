# Project Architecture
Documentation for the different directory in this project
## src
The vueJs code for the application is here.

#### views
The different pages of the application are here.

#### store
The store is here.

#### services
Services class are used to hold methods which are used at different places of the project (i.e: api call methods).

#### router
Router configuration is here

#### models
Models which represent formated data are stored here.

#### locales
Translations files are here.

#### components
The differents components for the views are here in 3 differents folders : "shared" for components reusable, character for components used to display info of one character, and character-list for components used to present multiple character.

#### assets
Image use in the code are stored here

## cypress
Cypress test files are here

## node_modules
Dependencies of the project are here.

## doc
Documentation of the project is in here.


