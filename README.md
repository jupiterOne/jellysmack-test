# jellysmack-test

This is a test app in Vue.js for Jellysmack, the aim of the app is to fetch characters of Rick&Morty through https://rickandmortyapi.com and to
show them in one page, the user should be able to change page of characters and to filter them by name or status and then on clicking on one character
to navigate on an info page of the character.  

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Cypress
```
$(npm bin)/cypress run
```
or, if you want to run cypress interactive GUI
```
$(npm bin)/cypress open
```
