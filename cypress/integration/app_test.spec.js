/// <reference types="cypress" />

describe('Test for app Rick&Morty', () => {
  beforeEach(() => {
    cy.visit('http://localhost:8080')
  });

  it('should load first page of all characters at launch', () => {
    cy.get('#character-card-1 .character-card-name').should('have.text', 'Rick Sanchez')
  })

  it('should click on pagination and goes to next page of charaters', () => {
    cy.get('#character-card-1 .character-card-name').should('have.text', 'Rick Sanchez')

    cy.get('.va-pagination').children().eq(3).click()

    cy.get('#character-card-21 .character-card-name').should('have.text', 'Aqua Morty')
  })

  it('should click on card button and go to character description', () => {
    cy.get('#character-card-1 .character-card-navigation').click()

    cy.url().should('equal', 'http://localhost:8080/characters/1')
  })

  it ('should fill form name and clear it after', () => {
    cy.get('#search-form-name').type('pickle').should('have.value', 'pickle')

    cy.get('.va-input__append-inner').click()

    cy.get('#search-form-name').should('not.have.value', 'pickle')
  })

  it ('should fill form and clear it after', () => {
    cy.get('#search-form-name').type('blue').should('have.value', 'blue')

    cy.get('#search-form-status').children().eq(1).click()

    cy.get('#search-form-reset').click()

    cy.get('#search-form-name').should('not.have.value', 'blue')

  })

  it ('shoudl fill form and launch search', () => {
    cy.get('#search-form-name').type('blue').should('have.value', 'blue')

    cy.get('#search-form-status').children().eq(1).click()

    cy.get('#search-form-submit').click()

    cy.get('#character-card-52 .character-card-name').should('have.text', 'Blue Footprint Guy')
  })
});