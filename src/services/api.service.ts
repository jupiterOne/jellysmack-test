import QueryParams from '@/models/query-params.model';
import * as axios from 'axios';

const BASE_URL = 'https://rickandmortyapi.com/api';

export default class RickAndMortyApi {
  // Method to fecth multiple character from Rick&Morty Api
  // params represents the filter of the query
  static async getMultipleCharacter(params: QueryParams) {
    try {
      const response = await axios.default.get(`${BASE_URL}/character/`, { params });
      return response.data;
    } catch (error) {
      return { results: [], info: { next: '' } };
    }
  }

  // Method to fetch one character from Rick&Morty Api
  // id represents the id of the character we want
  static async getSingleCharacter(id: number) {
    const response = await axios.default.get(`${BASE_URL}/character/${id}`);

    return response.data;
  }
}
