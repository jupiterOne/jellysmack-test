import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import CharacterList from '../views/CharacterListView.vue';
import CharacterDetail from '../views/CharacterDetailView.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'home',
    redirect: { name: 'list-characters' },
  },
  {
    path: '/characters/:id',
    name: 'detail-character',
    component: CharacterDetail,
    meta: {
      title: 'Détail Personnage',
      breadcrumb: [
        { name: 'Liste des personnages', link: 'list-characters' },
        { name: 'Détail Personnage', link: 'detail-character' },
      ],
    },
  },
  {
    path: '/characters',
    name: 'list-characters',
    component: CharacterList,
    meta: {
      title: 'Liste des personnages',
      breadcrumb: [
        { name: 'Liste des personnages', link: 'list-characters' },
      ],
    },
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
