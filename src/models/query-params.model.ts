interface QueryParams {
    gender: string,
    name: string,
    status: string,
    species: string,
    type: string,
    page: number,
}

export default QueryParams;
