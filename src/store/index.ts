import Character from '@/models/character.model';
import QueryParams from '@/models/query-params.model';
import RickAndMortyApi from '@/services/api.service';
import { createStore } from 'vuex';

interface AppState {
  charactersList: Array<Character>, // List of all characters for current page
  selectedCharacter: Character | null, // Current shown character
  lastQuery: QueryParams, // Last query sent to api
  totalPages: number, // Total pages for last query
  loading: boolean, // Is the app loading for character(s)
}

export default createStore({
  state() {
    return {
      page: 1,
      charactersList: [],
      selectedCharacter: null,
      lastQuery: { page: 1 } as QueryParams,
      totalPages: 1,
      loading: false,
    };
  },
  getters: {
    getCharactersList(state: AppState) {
      return state.charactersList;
    },

    getLastQuery(state: AppState) {
      return state.lastQuery;
    },

    getSelectedCharacter(state: AppState) {
      return state.selectedCharacter;
    },

    getTotalPages(state: AppState) {
      return state.totalPages;
    },

    getLoading(state: AppState) {
      return state.loading;
    },
  },
  mutations: {
    setCharactersList(state: AppState, payload: Array<Character>) {
      state.charactersList = payload;
    },

    appendNextCharacters(state: AppState, payload: Array<Character>) {
      state.charactersList = state.charactersList.concat(payload);
    },

    setLastQuery(state: AppState, payload: QueryParams) {
      state.lastQuery = payload;
    },

    setSelectedCharacter(state: AppState, payload: Character) {
      state.selectedCharacter = payload;
    },

    setTotalPages(state: AppState, payload) {
      state.totalPages = payload;
    },

    setLoading(state: AppState, payload) {
      state.loading = payload;
    },
  },
  actions: {
    fetchCharacters({ commit }) {
      try {
        RickAndMortyApi.getMultipleCharacter(this.state.lastQuery).then((response) => {
          commit('setCharactersList', response.results);
          commit('setTotalPages', response.info.pages);
        });
      } catch (error) {
        commit('setCharactersList', []);
        commit('setTotalPages', 0);
      }
    },

    fetchCharacter({ commit }, payload) {
      try {
        RickAndMortyApi.getSingleCharacter(payload).then((response) => {
          commit('setLoading', false);
          commit('setSelectedCharacter', response);
        });
      } catch (error) {
        commit('setLoading', false);
        commit('setSelectedCharacter', null);
      }
    },
  },
  modules: {
  },
});
