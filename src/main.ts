import { createApp } from 'vue';
import { VuesticPlugin } from 'vuestic-ui';
import App from './App.vue';
import router from './router';
import store from './store';
import 'vuestic-ui/dist/vuestic-ui.css';
import 'vuestic-ui/dist/styles/essential.css';
import i18n from './i18n';

createApp(App)
  .use(i18n)
  .use(store)
  .use(router)
  .use(VuesticPlugin)
  .mount('#app');
